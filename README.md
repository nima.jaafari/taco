# Taco
Taco (short for T(est) A(utomation) Co(mmons) is an amalgamation of all the helper functions you'll need when writing test automation at HomeAdvisor. It simplifies and standardizes all the basic scaffolding tasks that everyone ends up doing, like creating a new user before a test run or retrieving a random host name for a given micro-service. 

- [FAQ](#faq)
- [Getting Started](#getting-started)
  - [Environments](#environments)
- [API](#api)
  - [`getHost`](#gethost)
  - [`setHost`](#sethost)
  - [`generateUrl`](#generateurl)
  - [`generateHostUrl`](#generatehosturl)
  - [`createConsumer`](#createconsumer)
  - [`createServiceRequest`](#createservicerequest)
  - [`createSpPool`](#createsppool)
  - [`createSpPoolByMatchType`](#createsppoolbymatchtype)
  - [`getConfig`](#getconfig)
  - [`setConfig`](#setconfig)
- [Contributing](#contributing)

# FAQ
_What (test) frameworks does Taco work with?_<br/>
All of them! Taco was specifically written to be framework-agnostic. The only requirements are NodeJS/JavaScript (duh) and an HA VPN connection. 

_How to I limit the amount of external calls Taco makes?_<br/>
Taco will automatically cache relevant values so it doesn't do any more API calls than strictly necessary. E.g. the first time you call `getHost('potential-sr')` Taco will call into service discovery to get a random host name, but the second time the value will be retrieved from the cache. The cache is shared throughout the test suite and will reset at the end of the test run. 

_I rely on generic function X, but I can't seem to find it in Taco._<br/>
Technically, that's not a question, but if there's a function you feel everyone should have access to that isn't in the list below you should totally add it in! Check out [Contributing](#contributing) below for tips and tricks on how to get started. 

# Getting Started
Taco is published in HomeAdvisor's NPM registry, which you can access by adding `registry=http://npm.1800c.xyz/` (or some variation thereof) to a `.npmrc` file in your project's root directory.

Install
```
npm install @ha/taco
```
Import and use
```
import { createConsumer } from '@ha/taco';

createConsumer().then(consumer => console.log(consumer));
// Outputs a brand new HA consumer created through /resource/consumer/register
```

## Environments
Taco defaults to the sandbox environment. To target a different environment, call `setConfig()` with the desired environment (`sandbox`, `qa3`, or `qa1`) before you trigger anything else. 
```
import { setConfig, getConfig } from '@ha/taco';

setConfig('qa1');
createConsumer().then(consumer => console.log(consumer));
// Creates a user in QA1 instead of sandbox 
```

# API
## getHost
Retrieves the cached API address of the given micro-service if there is one, or will retrieve a random one if there isn't. Random addresses retrieved through the API will automatically be cached, so `getHost` will only perform a maximum of one API call per micro-service per test run, regardless of how often it is used or where in the test suite it is invoked.  
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| microServiceName | `string` | The target micro-service. |
### Example
```
getHost('potential-sr'); // du000kbw023.homeadvisor.com:30986
```

## setHost
Stores the address of a micro-service in the cache, so that subsequent [`getHost`](#gethost) calls that target the same micro-service will return the value set rather than retrieving a random host through an API. Should be used at the start of a test run if it is important that a specific micro-service instance be targeted. Note that random API addresses retrieved with `getHost` are automatically cached, so `setHost` should never be used to set a random address.
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| microServiceName | `string` | The target micro-service. |
| hostName | `string` | The host's full address. |
### Example
```
getHost('cost-guide');                                          // du000kbw023.homeadvisor.com:30986
setHost('cost-guide', 'du000kbw027.homeadvisor.com:31702');     // du000kbw027.homeadvisor.com:31702
getHost('cost-guide');                                          // du000kbw027.homeadvisor.com:31702    
```

## generateUrl
Appends all the standard HomeAdvisor bells and whistles to a URL so you don't have to. Defaults to `r_username`/`r_accesskey`, but can be configured to append `entity` information and other custom values. To generate a HomeAdvisor URL based on a micro-service host name rather than a full URL, try [`generateHostUrl`](#generatehosturl).
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| url | `string` | The base URL to append query parameters to. |
| options* | `IGenerateUrlOptions` | Customization options. |
_*optional_
#### IGenerateUrlOptions
| Name | Type | Details |
| ---- | ---- | ------- |
| newEntity* | `boolean` | Creates a new user and uses its values to set `entityId` and `entityHash`. |
| entity* | `{ entityId: string, entityHash: string }` | Set the `entityId` and `entityHash` to those of a specific, pre-existing consumer. |
| rUser* | `{ r_username: string, r_accesskey: string }` | Override the `r_user` information stored in the config. | 
| queryParameters* | `Object` | Extra custom query parameters to append to the URL. |
_*optional_
### Example
```
generateUrl('https://some-homeadvisor-url.com', { newEntity: true });
// https://some-homeadvisor-url.com?entityId=114907639&entityHash=114907639_sd16885adbf58d53e226f2b07ac487d34&r_username=api_test&r_accesskey=NEhR2ugM
generateUrl('https://some-homeadvisor-url.com', {
  entity: {
    entityId: 'an-entity-id',
    entityHash: 'an-entity-hash',
  },
  queryParameters: {
    test: 'check',
  },
  rUser: {
    r_username: 'yooser',
    r_accesskey: 'seeeeeeecret',
  },
});
// https://some-homeadvisor-url.com?test=check&entityId=an-entity-id&entityHash=an-entity-hash&r_username=yooser&r_accesskey=seeeeeeecret
```

## generateHostUrl
Converts a micro-service host name to a valid URL by appending all the standard HomeAdvisor bells and whistles. The host name is retrieved through [`getHost`](#gethost) and will take cached values into account. Defaults to `r_username`/`r_accesskey`, but can be configured to append `entity` information and other custom values. To target a URL rather than a micro-service host name, try [`generateUrl`](#generateurl).
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| microServiceName | `string` | The target micro-service. |
| endpoint | `string` | The desired endpoint (basically everything you want to append to the host name that isn't a query parameter). |
| options* | `IGenerateUrlOptions` | Customization options. |
_*optional_
#### IGenerateUrlOptions
| Name | Type | Details |
| ---- | ---- | ------- |
| newEntity* | `boolean` | Creates a new user and uses its values to set `entityId` and `entityHash`. |
| entity* | `{ entityId: string, entityHash: string }` | Set the `entityId` and `entityHash` to those of a specific, pre-existing consumer. |
| rUser* | `{ r_username: string, r_accesskey: string }` | Override the `r_user` information stored in the config. | 
| queryParameters* | `Object` | Extra custom query parameters to append to the URL. |
_*optional_
### Example
```
generateHostUrl('fulfill-external', '/resource/fulfill-external/getTaskWhitelistForReason', { newEntity: true });
// http://du000kbw035.homeadvisor.com:30074/resource/fulfill-external/getTaskWhitelistForReason?test=check&entityId=an-entity-id&entityHash=an-entity-hash&r_username=yooser&r_accesskey=seeeeeeecret
generateHostUrl('fulfill-external', '/resource/fulfill-external/getTaskWhitelistForReason', {
  entity: {
    entityId: 'an-entity-id',
    entityHash: 'an-entity-hash',
  },
  queryParameters: {
    test: 'check',
  },
  rUser: {
    r_username: 'yooser',
    r_accesskey: 'seeeeeeecret',
  },
});
// http://du000kbw040.homeadvisor.com:30074/resource/fulfill-external/getTaskWhitelistForReason?entityId=114907654&entityHash=114907654_sd6b614bee6daaaa2af1c6a4799e16678&r_username=api_test&r_accesskey=NEhR2ugM
```


## createConsumer
Asynchronously creates and returns a consumer based on the default values stored in Taco's config. Note that the resulting consumer is _not_ cached, as it is assumed each test should start with a clean slate. 
### Example
```
(await createConsumer()).consumer.entityId;   // 114907549
```

## createServiceRequest
Asynchronously creates and returns an SR based on the default values stored in Taco's config. Note that the resulting SR is _not_ cached, as it is assumed each test should start with a clean slate. 
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| taskOid | `number` | The taskOid to create an SR for. |
| options* | `ICreateServiceRequestOptions` | Customization options |
_*optional_
### ICreateServiceRequestOptions
| Name | Type | Details |
| ---- | ---- | ------- |
| consumer | `IConsumer` | The SR's submitter. A new consumer will be created and returned if this is left blank. |
### Example
```
(await createServiceRequest(40006)).srOid;    // 208689701
```

## createSpPool
Clears and repopulates the SP pool for a given taskOid with the SPs passed. The API allows for a lot of customization, but will replace all missing properties with valid defaults. If the specific pros aren't as important as the resulting match type, consider using [createSpPoolByMatchType](#createSpPoolByMatchType) instead. 
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| pros | `IServiceProfessional[]` | The list of SPs to populate the pro pool with. |
| taskOid | `number` | The taskOid of the desired pro pool. |
| zip* | `number` | The zip of the desired pro pool. Defaults to the value stored in Taco's `config.consumer.zip`. |
_*optional_
## Example
```
createSpPool([{ companyName: 'X-treme Vacuumers' }], 40006, 98034);
// Reset the maid pro pool for zip 98034 with a single company name that ostensibly specializes in vacuuming 
```

## createSpPoolByMatchType
Clears and repopulates the SP pool for a given taskOid with three SPs that fulfill the requirement of the given match type. For more granular control over the resulting pro pool, consider [createSpPool](#createSpPool).
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| matchType | `'CC' \| 'OL' \| 'bronze' \| 'empty'` | The match type to adhere to. Defaults to CC. |
| taskOid | `number` | The taskOid of the desired pro pool. |
| zip* | `number` | The zip of the desired pro pool. Defaults to the value stored in Taco's `config.consumer.zip`. |
_*optional_
## Example
```
createSpPoolByMatchType('OL', 40113);
// Populates the faucet repair pro pool for the default zip with three random OL pros
```

## getConfig
Retrieves Taco's config object from the cache if available or generates a new config object for the `sandbox` environment if not. The config object contains the default values for all of Taco's methods. Use [`setConfig`](#setconfig) to change Taco's default environment.
## Example
```
getConfig().consumer.address;   // 13210 97th Ave
```

## setConfig
Changes Taco's target environment and returns the matching config object. By default, [`getConfig`](#getconfig) will generate a config object with values specific to HomeAdvisor's `sandbox` environment that are used as defaults by Taco's various methods. Running `setConfig('qa1')` in a root `before` during your test run will ensure all invocations of Taco's methods will target QA1 instead. 
### Parameters
| Name | Type | Details |
| ---- | ---- | ------- |
| env | `'sandbox' \| 'qa3' \| 'qa1'` | Target environment. Defaults to `sandbox`. |
_*optional_
## Example
```
getConfig().env;        // sandbox
setConfig('qa3').env;   // qa3
getConfig().env;        // qa3
```

# Contributing
Everyone is welcome (and even encouraged!) to contribute, but there are a few ground rules. 
## Rules
- Make sure to run the linter (`npm run lint`) and the compiler (`npm run build`) before you submit an MR. 
- Breaking changes to API contracts should be avoided if possible and discussed before submission if not. 
- Strict typing is required for API signatures.
- All APIs should be accurately documented in this README.
## Tips
- Consider abstracting complex API signatures, with a large number of optional or granular parameters, into a single `options` parameter. This allows for named parameters for transparency, as well as skipping optional parameters. 
- Values retrieved through an API that might be used more than once throughout a test run should be cached for efficiency. This applies to immutable values (such as a micro-service host name) but not to values that might be contaminated when used in a test (such as a consumer). 