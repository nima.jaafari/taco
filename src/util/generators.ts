import { getConfig, IConfigConsumer } from '../lib/config';

// Generates a (semi-)random number with the specified number of digits that
// does not start with zero.
export function generateRandomNumber(numberOfDigits = 6): number {
  let factor = 1;
  for (let i = numberOfDigits - 2; i >= 0; i--) {
    factor = factor * 10;
  }
  return Math.floor(factor + Math.random() * (9 * factor));
}

export function generateConsumer() {
  const consumer = getConfig().consumer;
  consumer['email'] = `gigApiTests${new Date().toISOString().replace(/:/g, '.')}@edify.com`;
  return consumer as IPreSubmitConsumer;
}

export interface IPreSubmitConsumer extends IConfigConsumer {
  email: string;
}
