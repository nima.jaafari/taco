import { get } from '../util/calls';
import { getConfig } from './config';
import { createConsumer } from './createConsumer';
import { IRUser } from './config';
import ValueCache from '../util/ValueCache';

class ApiAddressCache extends ValueCache<string> {
  microServiceName: string;

  constructor(microServiceName: string) {
    super(`${microServiceName}-host-name`);
    this.microServiceName = microServiceName;
  }
}

export function setHost(microServiceName: string, hostName: string): void {
  new ApiAddressCache(microServiceName).set(hostName);
}

export async function getHost(microServiceName: string): Promise<string> {
  const cache = new ApiAddressCache(microServiceName);
  return cache.get() || cacheHost(cache);
}

async function cacheHost(cache: ApiAddressCache): Promise<string> {
  const host = await getHostFromKomodo(cache.microServiceName);
  cache.set(host);
  return host;
}

async function getHostFromKomodo(microServiceName: string): Promise<string> {
  const url = `http://${getConfig().env}-komodo.homeadvisor.com/api/curator/v2/bestservice/${microServiceName}`;
  const response = await get(url, { errorMessage: `Failed to establish stable container for ${microServiceName}` });
  return response.location;
}

export async function generateHostUrl(microServiceName: string, endpoint: string, options?: IGenerateUrlOptions): Promise<string> {
  return generateUrl(`http://${await getHost(microServiceName)}${endpoint}`, options);
}

export async function generateUrl(baseUrl: string, options?: IGenerateUrlOptions): Promise<string> {
  const queryParams: Record<string, string | number | boolean> = options?.queryParameters || {};

  if (options?.entity) {
    queryParams.entityId = options.entity.entityId;
    queryParams.entityHash = options.entity.entityHash;
  }

  if (options?.newEntity) {
    const newEntity = await createConsumer();
    queryParams.entityId = newEntity.entityId;
    queryParams.entityHash = newEntity.entityHash;
  }

  const rUser = options?.rUser || getConfig().rUser;
  queryParams.r_username = rUser.r_username;
  queryParams.r_accesskey = rUser.r_accesskey;

  let queryString = '';
  for (const [key, value] of Object.entries(queryParams)) {
    queryString += `&${key}=${value}`;
  }

  if (baseUrl.indexOf('?') == -1) {
    queryString = `?${queryString.substr(1)}`;
  }

  return `${baseUrl}${queryString}`;
}

export interface IGenerateUrlOptions {
  queryParameters?: Record<string, string | number | boolean>;
  entity?: {
    entityId: string;
    entityHash: string;
  };
  newEntity?: true;
  rUser?: IRUser;
}
