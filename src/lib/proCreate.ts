import { post } from '../util/calls';
import { getConfig } from './config';
import { generateUrl } from './urlHelpers';

export async function createSpPool(pros: IServiceProfessional[], taskOid: number, zip?: number) {
  const config = getConfig();
  const url = await generateUrl(`https://${config.env}-api.homeadvisor.com/resource/automation/procreate`);
  return post(url, {
    data: {
      taskOid,
      zipCode: zip || config.consumer.zip,
      pros,
      cleanPool: true,
    },
    errorMessage: 'HomeAdvisor procreation failed',
  });
}

export function createSpPoolByMatchType(matchType: MatchType, taskOid: number, zip?: number) {
  const pros = generatePros(validateMatchType(matchType));
  return createSpPool(pros, taskOid, zip);
}

function validateMatchType(matchType: string): MatchType {
  const matchTypes = ['OL', 'CC', 'bronze'];
  if (matchTypes.indexOf(matchType) == -1) {
    throw new Error(`Invalid match type; supported options are ${matchTypes.join(', ')}`);
  }
  return matchType as MatchType;
}

function generatePros(matchType: MatchType): IServiceProfessional[] {
  switch (matchType) {
    case 'CC':
    case 'OL':
      return [
        generatePro('The Amazing Test Pros!', matchType),
        generatePro('Tests-R-Us Pros', matchType),
        generatePro('Test ALL the Things Inc', matchType),
      ];
    case 'bronze':
    case 'empty':
      return [];
  }
}

function generatePro(companyName: string, matchType: MatchType): IServiceProfessional {
  const sp: IServiceProfessional = {
    companyName,
    enrollmentStatus: 3,
    enrollmentSubStatus: 1,
    activeForMatch: true,
    xmCap: 90000,
    mmCap: 100000,
    acceptType: 2,
    xmSp: false,
    instantConnectSp: false,
    sdbSp: false,
    scheduable: false,
    membership: true,
    testSp: true,
    sweeper: false,
    userAttributeTypeIds: [7],
  };

  if (matchType == 'OL') {
    sp.olEligible = true;
    sp.xmCap = 0;
    sp.mmCap = 0;
  }

  return sp;
}

export interface IServiceProfessional {
  companyName?: string;
  enrollmentStatus?: number;
  enrollmentSubStatus?: number;
  activeForMatch?: boolean;
  xmCap?: number;
  mmCap?: number;
  acceptType?: number;
  xmSp?: boolean;
  instantConnectSp?: boolean;
  sdbSp?: boolean;
  scheduable?: boolean;
  membership?: boolean;
  testSp?: boolean;
  sweeper?: boolean;
  userAttributeTypeIds?: number[];
  olEligible?: boolean;
  offerCode?: string;
}

export type MatchType = 'OL' | 'CC' | 'bronze' | 'empty';
