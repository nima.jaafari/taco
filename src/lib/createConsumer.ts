import { generateConsumer, IPreSubmitConsumer } from '../util/generators';
import { getConfig } from './config';
import { generateUrl } from './urlHelpers';
import { post } from '../util/calls';

export async function createConsumer(): Promise<IConsumer> {
  const config = getConfig();
  const url = await generateUrl(`https://${config.env}-api.homeadvisor.com/resource/consumer/register`);
  const preSubmitConsumer = generateConsumer();

  const rawConsumer = await post(url, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    form: {
      email: preSubmitConsumer.email,
      zip: preSubmitConsumer.zip,
      password: preSubmitConsumer.password,
      firstName: preSubmitConsumer.firstName,
      lastName: preSubmitConsumer.lastName,
      cellPhone: preSubmitConsumer.phone,
      affEntryPointID: 1,
    },
    errorMessage: 'Failed to create new consumer',
  });

  return {
    rawConsumer,
    entityId: rawConsumer.entityId,
    entityHash: rawConsumer.entityHash,
    firstName: rawConsumer.firstName,
    lastName: rawConsumer.lastName,
    city: rawConsumer.primaryCity,
    state: rawConsumer.primaryState,
    zip: rawConsumer.primaryZip,
    // consumer/register does not return all properties, so several are filled by the initial consumer object.
    address: preSubmitConsumer.address,
    phone: preSubmitConsumer.phone,
    country: preSubmitConsumer.country,
    password: preSubmitConsumer.password,
    email: preSubmitConsumer.email,
  };
}

export interface IConsumer extends IPreSubmitConsumer {
  rawConsumer?: any;
  entityId: string;
  entityHash: string;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  address: string;
  city: string;
  zip: string;
  state: string;
  country: string;
  password: string;
}
