import ValueCache from '../util/ValueCache';

class ConfigCache extends ValueCache<IConfig> {
  constructor() {
    super('taco-config');
  }
}

export function getConfig(): IConfig {
  return new ConfigCache().get() || setConfig();
}

export function setConfig(env: Env = 'sandbox', customValues?: Record<string, any>): IConfig {
  let config = Object.assign({}, require('../../configs/base.conf.json'), getEnvConf(env));
  if (customValues) {
    config = Object.assign({}, config, customValues);
  }
  new ConfigCache().set(config);
  return config;
}

function getEnvConf(env: Env) {
  switch (env) {
    case 'sandbox': return require('../../configs/sandbox.conf.json');
    case 'qa3': return require('../../configs/qa3.conf.json');
    case 'qa1': return require('../../configs/qa1.conf.json');
    default: throw new Error(`Invalid env <${env}>: either designate the target fulfill-external host name directly in SERVICE_HOSTNAME/SERVICE_PORT, or set RANDOM_ENV to sandbox, qa3, or qa1`);
  }
}

export interface IConfig extends Record<string, any> {
  env: 'sandbox' | 'qa3' | 'qa3';
  rUser: IRUser;
  consumer: IConfigConsumer;
  handyProviderId: number;
}

export interface IRUser {
  r_username: string;
  r_accesskey: string;
}

export interface IConfigConsumer {
  firstName: string;
  lastName: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  country: string;
  password: string;
}

export type Env = 'sandbox' | 'qa3' | 'qa1';
