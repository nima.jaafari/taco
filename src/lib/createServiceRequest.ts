import { generateConsumer, IPreSubmitConsumer } from '../util/generators';
import { IConsumer } from './createConsumer';
import { post } from '../util/calls';
import { generateHostUrl } from './urlHelpers';

export async function createServiceRequest(taskOid, options?: ICreateServiceRequestOptions): Promise<IServiceRequest> {
  const url = await generateHostUrl('potential-sr', '/resource/sr-creation');
  const consumer = options?.consumer || generateConsumer();

  const rawServiceRequest = await post(url, {
    data: {
      matchingSource: 'SITE',
      matchingSourceId: 1,
      entryPointId: 0,
      specificRequestDto: {
        taskOid,
        originalTaskOid: taskOid,
        consumer: {
          firstName: consumer.firstName,
          lastName: consumer.lastName,
          email: consumer.email,
          phone: consumer.phone,
          city: consumer.city,
          state: consumer.state,
          postalCode: consumer.zip,
        },
        questionSetId: 603,
        originalQuestionSetId: 603,
        questionsAndAnswers: {
          7735: 20767,
          8373: 67562,
          80000: 11149,
        },
      },
    },
    errorMessage: 'SR request failed',
  });

  if (!options?.consumer) {
    consumer['entityId'] = rawServiceRequest.consumer.entityId;
    consumer['entityHash'] = rawServiceRequest.consumer.entityHash;
  }

  return {
    rawServiceRequest,
    srOid: rawServiceRequest.serviceRequest.serviceOId,
    consumer: consumer as IConsumer,
  };
}

export interface ICreateServiceRequestOptions {
  consumer?: IPreSubmitConsumer;
}

export interface IServiceRequest {
  rawServiceRequest: any;
  srOid: number;
  consumer: IConsumer;
}
